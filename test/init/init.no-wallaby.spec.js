import fs from "fs"
import path from "path"
import execa from "execa"
import stripColor from "strip-color"
import test from "jest-t-assert"
import { split, pipe } from "f-utility"
// import { j2 } from './utils'
const FILE = `${__dirname}/.gitpartyrc`

jasmine.DEFAULT_TIMEOUT_INTERVAL = 1e5

const cleanify = pipe(
  stripColor,
  x => split(`80ca7f7`, x)[1],
  split(`\n`)
)
const CLI = path.resolve(__dirname, `../../lib/index.js`)

test.cb(`gitparty --init`, t => {
  t.plan(1)
  execa.shell(`node ${CLI} --init`).then(x => {
    t.is(x.stdout, `Created a .gitpartyrc file`)
    t.end()
  })
})

test.cb(`gitparty --init and then gitparty`, t => {
  t.plan(2)
  execa.shell(`node ${CLI} --init`).then(x => {
    t.is(x.stdout, `Created a .gitpartyrc file`)
    return execa.shell(`node ${CLI}`).then(y => {
      t.deepEqual(cleanify(y.stdout), [
        ` - fixed that hilarious problem of the tests never be... $ brekk   | js`,
        `                  10-05-2018                                                                                            `,
        `       T           = 9bd10f4 - committing anything breaks the existing tests :joy... $ brekk   | js`,
        `       T           = 5e131fb - passing tests again                                   $ brekk   | js`,
        ` G     T  R  D  C  = 8a4f3a9 - nearing 100% coverage                                 $ brekk   | gitpartyrc js json lock`,
        `                  09-05-2018                                                                                            `,
        ` G  L  T           = 4661430 - added a readme                                        $ brekk   | eslintrc js md png`,
        ` G                 = 9210f41 - refactor                                              $ brekk   | js`,
        `                  08-05-2018                                                                                            `,
        ` G                 = f64e517 - everything futurized but currently requires double... $ brekk   | js`,
        ` G                 = aaa4e6c - more fluturization                                    $ brekk   | js`,
        ` G                 = 40e3dbc - added fluture and clarifying intent                   $ brekk   | js`,
        ` G           D  C  = c7f3eb7 - moving things around for the bin script               $ brekk   | js json`,
        ` G     T     D  C  = 8886271 - this will likely break non-relative tests             $ brekk   | js json lock`,
        ` G  L  T  R        = fb50fbb - tests                                                 $ brekk   | eslintrc gitpartyrc js json`,
        `                  07-05-2018                                                                                            `,
        ` G        R        = fa928f4 - gitpartyrc                                            $ brekk   | gitpartyrc js yml`,
        ` G           D  C  = f9e5c4f - added yaml config                                     $ brekk   | js json lock yml`,
        ` G                 = 925a86e - getting cleaner                                       $ brekk   | js`,
        ` G           D  C  = c2e257b - working again                                         $ brekk   | js json lock yml`,
        `                  03-05-2018                                                                                            `,
        ` G           D  C  = b9d98d9 - process.cwd() over __dirname                          $ brekk   | js json`,
        `                  02-05-2018                                                                                            `,
        ` G                 = d9249be - pass lint                                             $ brekk   | js`,
        `                   = c5f2766 - added some date grossness for now                     $ brekk   | js`,
        ` G                 = e9569fb - pretty close to ready                                 $ brekk   | js`,
        `       T           = f3cc824 - utils covered                                         $ brekk   | js`,
        ` G     T     D  C  = 08c0a46 - tests!                                                $ brekk   | js json lock`,
        ` G  L              = 56b6a81 - cleanification                                        $ brekk   | eslintrc js`,
        ` G                 = 8ea8fe8 - partial change                                        $ brekk   | js`,
        `                  01-05-2018                                                                                            `,
        ` G                 = bb2def9 - getting cleaner, legend can be extricated             $ brekk   | js`,
        ` G                 = 21f377f - legend is almost fully extracted                      $ brekk   | js`,
        ` G           D  C  = 852f7ac - add blob matching, start to clean up legend makery    $ brekk   | js json lock`,
        ` G           D  C  = 322b8d0 - cleanups and more fp                                  $ brekk   | js json lock`,
        ` G                 = c182937 - commit it while it works, dammit                      $ brekk   | js`,
        ` G           D  C  = 58b0786 - better                                                $ brekk   | js json lock`,
        ` G  L     R  D  C  = 1c5ffd2 - initial commit                                        $ brekk   | babelrc eslintrc gitignore js json lock madgerc npmignore yml`
      ])
      t.end()
    })
    // t.end()
  })
})

afterEach(() => execa.shell(`rm -f ${FILE}`))
