import fs from "fs"
import path from "path"
import execa from "execa"
import stripColor from "strip-color"
import test from "jest-t-assert"
import { split, pipe } from "f-utility"
const FILE = `${__dirname}/.gitpartyrc`

const cleanify = pipe(
  stripColor,
  x => split(`first?`, x)[0]
)
const CLI = `../../lib/index.js`

test.cb(`gitparty with no git`, t => {
  t.plan(1)
  process.chdir(__dirname)
  // console.log(`dirname????`, __dirname)
  execa.shell(`node ${CLI}`).then(x => {
    t.is(
      cleanify(x.stderr),
      `Error: gitparty only works in git repositories! Did you mean to \`git init\` `
    )
    // t.falsy(fs.existsSync(FILE))
    t.end()
  })
})
// TODO: figure out why exactly this keeps getting created, afaict it's the test
afterEach(() => execa.shell(`rm -f ${FILE}`))
