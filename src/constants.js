import chalk from "chalk"

export const TOTAL_COMMITS = 100
export const SUBJECT_LENGTH = 50
export const BANNER_LENGTH = 120
export const BANNER_INDENT = 28
export const AUTHOR_LENGTH = 7
export const DEFAULT_FIELDS = [
  `abbrevHash`,
  `subject`,
  `authorName`,
  `authorDate`,
  `authorDateRel`
]

export const PARTYFILE = `.gitpartyrc`

export const DEFAULT_CONFIG = {
  // this is our config
  filterMergeCommits: true,
  collapseAuthors: false,
  authorLength: AUTHOR_LENGTH,
  subjectLength: SUBJECT_LENGTH,
  bannerLength: BANNER_LENGTH,
  bannerIndent: BANNER_INDENT,
  filter: ``,
  json: false,
  // below this line are gitlog configuration
  repo: process.cwd(),
  number: TOTAL_COMMITS,
  fields: DEFAULT_FIELDS,
  timezone: `UTC`,
  execOptions: { maxBuffer: 1000 * 1024 }
}

export const CHARACTER_LITERALS = {
  BLANK: ` `,
  NEWLINE: `\n`,
  ASTERISK: `*`,
  TILDE: /~$/
}

export const ARGV_CONFIG = {
  boolean: [`m`, `a`, `i`],
  number: [`l`, `e`, `b`, `s`],
  alias: {
    a: [`collapseAuthors`, `collapse`],
    b: `bannerLength`,
    c: `config`,
    f: `filter`,
    h: `help`,
    e: `bannerIndent`,
    i: `init`,
    j: `json`,
    l: `authorLength`,
    m: `filterMergeCommits`,
    o: `output`,
    s: `subjectLength`,
    t: `timezone`,
    repo: `r`,
    number: [`n`, `totalCommits`]
  }
}

export const MAKE_A_GITPARTYRC_FILE = [
  `Unable to find a ${PARTYFILE} config file in this folder.`,
  `Did you mean to create one with \`${chalk.yellow(`gitparty --init`)}\`?`
].join(` `)

// eslint-disable-next-line max-len
export const IT_ONLY_WORKS_IN_GIT_REPOS = `gitparty only works in git repositories! Did you mean to \`${chalk.yellow(
  `git init`
)}\` first?`

export const BASE_CONFIG_FILE = `gitpartyrc:
  key: G
  color: bgRed
  matches:
  - \\**${PARTYFILE}
collapseAuthors: false
timezone: utc
`
