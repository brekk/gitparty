import chalk from "chalk"
import { pipe, keys, map, join, curry } from "f-utility"
import pad from "pad"
// import { sideEffect, trace } from "xtrace"
import {
  CHARACTER_LITERALS,
  SUBJECT_LENGTH,
  AUTHOR_LENGTH,
  BANNER_LENGTH,
  BANNER_INDENT
} from "./constants"
import { filetypes } from "./per-commit"
import { summarize, preferredProp, lens } from "./utils"
import { getCanon } from "./alias"

const { BLANK } = CHARACTER_LITERALS

const padCharsStart = curry((length, str) => pad(length, str, BLANK))
const padCharsEnd = curry((length, str) => pad(str, length, BLANK))

export const drawToken = curry((lookup, analysis, name) => {
  // cold medina
  const { fn, key } = lookup[name]
  return analysis[name] ? chalk.black(fn(` ${key} `)) : `   `
})

export const drawTokens = curry((lookup, analysis) =>
  pipe(
    keys,
    map(drawToken(lookup, analysis)),
    join(``)
  )(lookup)
)

export const configureAndPrintBanner = curry((lookup, config, { date }) => {
  const grab = preferredProp(config, lookup)
  const bannerIndent = grab(BANNER_INDENT, `bannerIndent`)
  const bannerLength = grab(BANNER_LENGTH, `bannerLength`)
  return chalk.inverse(
    padCharsEnd(bannerLength, padCharsStart(bannerIndent, date))
  )
})

export const printAuthor = curry((author, length) =>
  pipe(
    getCanon,
    padCharsEnd(length),
    chalk.red
  )(author)
)
const printTypes = pipe(
  filetypes,
  join(BLANK)
)

const prepend = curry((str, str2) => [str, str2].join(BLANK))

export const formatHash = pipe(
  prepend(`=`),
  chalk.yellow
)
export const formatAuthor = curry((grab, author) =>
  pipe(
    grab(AUTHOR_LENGTH),
    printAuthor(author),
    prepend(`$`)
  )(`authorLength`)
)

export const formatSubject = curry((grab, subject) =>
  pipe(
    grab(SUBJECT_LENGTH),
    summarize(subject),
    prepend(`-`)
  )(`subjectLength`)
)

export const formatChanges = pipe(
  printTypes,
  prepend(`|`)
)

export const configureAndPrintCommit = curry((lookup, config, o) => {
  const grab = preferredProp(config, lookup)
  return pipe(
    lens(drawTokens(lookup), `analysis`),
    lens(formatHash, `hash`),
    lens(formatAuthor(grab), `author`),
    lens(formatSubject(grab), `subject`),
    lens(formatChanges, `changes`),
    ({ analysis, hash, subject, author, changes }) =>
      [analysis, hash, subject, author, changes].join(BLANK)
  )(o)
})

export const colorize = curry((config, lookup, raw) =>
  (raw.type === `banner` ? configureAndPrintBanner : configureAndPrintCommit)(
    lookup,
    config,
    raw
  )
)
