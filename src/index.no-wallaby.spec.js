import path from "path"
import execa from "execa"
import stripColor from "strip-color"
import test from "jest-t-assert"
import { map, split, pipe } from "f-utility"
// import { j2 } from './utils'

const cleanify = pipe(
  stripColor,
  x => split(`80ca7f7`, x)[1],
  split(`\n`)
)
const CLI = path.resolve(__dirname, `../lib/index.js`)

test.cb(`gitparty`, t => {
  t.plan(1)
  execa.shell(`node ${CLI} -l 7 --no-collapse`).then(x => {
    /* eslint-disable max-len */
    t.deepEqual(cleanify(x.stdout), [
      ` - fixed that hilarious problem of the tests never be... $ brekk   | js`,
      `                  10-05-2018                                                                                            `,
      `       T           = 9bd10f4 - committing anything breaks the existing tests :joy... $ brekk   | js`,
      `       T           = 5e131fb - passing tests again                                   $ brekk   | js`,
      ` G     T  R  D  C  = 8a4f3a9 - nearing 100% coverage                                 $ brekk   | gitpartyrc js json lock`,
      `                  09-05-2018                                                                                            `,
      ` G  L  T           = 4661430 - added a readme                                        $ brekk   | eslintrc js md png`,
      ` G                 = 9210f41 - refactor                                              $ brekk   | js`,
      `                  08-05-2018                                                                                            `,
      ` G                 = f64e517 - everything futurized but currently requires double... $ brekk   | js`,
      ` G                 = aaa4e6c - more fluturization                                    $ brekk   | js`,
      ` G                 = 40e3dbc - added fluture and clarifying intent                   $ brekk   | js`,
      ` G           D  C  = c7f3eb7 - moving things around for the bin script               $ brekk   | js json`,
      ` G     T     D  C  = 8886271 - this will likely break non-relative tests             $ brekk   | js json lock`,
      ` G  L  T  R        = fb50fbb - tests                                                 $ brekk   | eslintrc gitpartyrc js json`,
      `                  07-05-2018                                                                                            `,
      ` G        R        = fa928f4 - gitpartyrc                                            $ brekk   | gitpartyrc js yml`,
      ` G           D  C  = f9e5c4f - added yaml config                                     $ brekk   | js json lock yml`,
      ` G                 = 925a86e - getting cleaner                                       $ brekk   | js`,
      ` G           D  C  = c2e257b - working again                                         $ brekk   | js json lock yml`,
      `                  03-05-2018                                                                                            `,
      ` G           D  C  = b9d98d9 - process.cwd() over __dirname                          $ brekk   | js json`,
      `                  02-05-2018                                                                                            `,
      ` G                 = d9249be - pass lint                                             $ brekk   | js`,
      `                   = c5f2766 - added some date grossness for now                     $ brekk   | js`,
      ` G                 = e9569fb - pretty close to ready                                 $ brekk   | js`,
      `       T           = f3cc824 - utils covered                                         $ brekk   | js`,
      ` G     T     D  C  = 08c0a46 - tests!                                                $ brekk   | js json lock`,
      ` G  L              = 56b6a81 - cleanification                                        $ brekk   | eslintrc js`,
      ` G                 = 8ea8fe8 - partial change                                        $ brekk   | js`,
      `                  01-05-2018                                                                                            `,
      ` G                 = bb2def9 - getting cleaner, legend can be extricated             $ brekk   | js`,
      ` G                 = 21f377f - legend is almost fully extracted                      $ brekk   | js`,
      ` G           D  C  = 852f7ac - add blob matching, start to clean up legend makery    $ brekk   | js json lock`,
      ` G           D  C  = 322b8d0 - cleanups and more fp                                  $ brekk   | js json lock`,
      ` G                 = c182937 - commit it while it works, dammit                      $ brekk   | js`,
      ` G           D  C  = 58b0786 - better                                                $ brekk   | js json lock`,
      ` G  L     R  D  C  = 1c5ffd2 - initial commit                                        $ brekk   | babelrc eslintrc gitignore js json lock madgerc npmignore yml`
    ])
    t.end()
  })
  /* eslint-enable max-len */
})

test.cb(`gitparty --authorLength 15`, t => {
  t.plan(1)
  execa.shell(`node ${CLI} --authorLength 15`).then(x => {
    /* eslint-disable max-len */
    t.deepEqual(cleanify(x.stdout), [
      ` - fixed that hilarious problem of the tests never be... $ brekk           | js`,
      `                  10-05-2018                                                                                            `,
      `       T           = 9bd10f4 - committing anything breaks the existing tests :joy... $ brekk           | js`,
      `       T           = 5e131fb - passing tests again                                   $ brekk           | js`,
      ` G     T  R  D  C  = 8a4f3a9 - nearing 100% coverage                                 $ brekk           | gitpartyrc js json lock`,
      `                  09-05-2018                                                                                            `,
      ` G  L  T           = 4661430 - added a readme                                        $ brekk           | eslintrc js md png`,
      ` G                 = 9210f41 - refactor                                              $ brekk           | js`,
      `                  08-05-2018                                                                                            `,
      ` G                 = f64e517 - everything futurized but currently requires double... $ brekk           | js`,
      ` G                 = aaa4e6c - more fluturization                                    $ brekk           | js`,
      ` G                 = 40e3dbc - added fluture and clarifying intent                   $ brekk           | js`,
      ` G           D  C  = c7f3eb7 - moving things around for the bin script               $ brekk           | js json`,
      ` G     T     D  C  = 8886271 - this will likely break non-relative tests             $ brekk           | js json lock`,
      ` G  L  T  R        = fb50fbb - tests                                                 $ brekk           | eslintrc gitpartyrc js json`,
      `                  07-05-2018                                                                                            `,
      ` G        R        = fa928f4 - gitpartyrc                                            $ brekk           | gitpartyrc js yml`,
      ` G           D  C  = f9e5c4f - added yaml config                                     $ brekk           | js json lock yml`,
      ` G                 = 925a86e - getting cleaner                                       $ brekk           | js`,
      ` G           D  C  = c2e257b - working again                                         $ brekk           | js json lock yml`,
      `                  03-05-2018                                                                                            `,
      ` G           D  C  = b9d98d9 - process.cwd() over __dirname                          $ brekk           | js json`,
      `                  02-05-2018                                                                                            `,
      ` G                 = d9249be - pass lint                                             $ brekk           | js`,
      `                   = c5f2766 - added some date grossness for now                     $ brekk           | js`,
      ` G                 = e9569fb - pretty close to ready                                 $ brekk           | js`,
      `       T           = f3cc824 - utils covered                                         $ brekk           | js`,
      ` G     T     D  C  = 08c0a46 - tests!                                                $ brekk           | js json lock`,
      ` G  L              = 56b6a81 - cleanification                                        $ brekk           | eslintrc js`,
      ` G                 = 8ea8fe8 - partial change                                        $ brekk           | js`,
      `                  01-05-2018                                                                                            `,
      ` G                 = bb2def9 - getting cleaner, legend can be extricated             $ brekk           | js`,
      ` G                 = 21f377f - legend is almost fully extracted                      $ brekk           | js`,
      ` G           D  C  = 852f7ac - add blob matching, start to clean up legend makery    $ brekk           | js json lock`,
      ` G           D  C  = 322b8d0 - cleanups and more fp                                  $ brekk           | js json lock`,
      ` G                 = c182937 - commit it while it works, dammit                      $ brekk           | js`,
      ` G           D  C  = 58b0786 - better                                                $ brekk           | js json lock`,
      ` G  L     R  D  C  = 1c5ffd2 - initial commit                                        $ brekk           | babelrc eslintrc gitignore js json lock madgerc npmignore yml`
    ])
    t.end()
  })
  /* eslint-enable max-len */
})

test.cb(`gitparty --collapse`, t => {
  t.plan(1)
  execa.shell(`node ${CLI} -a`).then(x => {
    /* eslint-disable max-len */
    t.deepEqual(cleanify(x.stdout), [
      ` - fixed that hilarious problem of the tests never be... $ brekk   | js`,
      `                  10-05-2018                                                                                            `,
      ` G     T  R  D  C  = 8a4f3a9 - committing anything breaks the existing tests :joy... $ brekk   | gitpartyrc js json lock`,
      `                  09-05-2018                                                                                            `,
      ` G  L  T           = 9210f41 - added a readme + refactor                             $ brekk   | eslintrc js md png`,
      `                  08-05-2018                                                                                            `,
      ` G  L  T  R  D  C  = fb50fbb - everything futurized but currently requires double... $ brekk   | eslintrc gitpartyrc js json lock`,
      `                  07-05-2018                                                                                            `,
      ` G        R  D  C  = c2e257b - gitpartyrc + added yaml config + getting cleaner +... $ brekk   | gitpartyrc js json lock yml`,
      `                  03-05-2018                                                                                            `,
      ` G           D  C  = b9d98d9 - process.cwd() over __dirname                          $ brekk   | js json`,
      `                  02-05-2018                                                                                            `,
      ` G  L  T     D  C  = 8ea8fe8 - pass lint + added some date grossness for now + pr... $ brekk   | eslintrc js json lock`,
      `                  01-05-2018                                                                                            `,
      ` G  L     R  D  C  = 1c5ffd2 - getting cleaner, legend can be extricated + legend... $ brekk   | babelrc eslintrc gitignore js json lock madgerc npmignore yml`
    ])
    t.end()
  })
  /* eslint-enable max-len */
})

test.cb(`gitparty -f hash:1c5ffd2 -j`, t => {
  t.plan(1)
  execa.shell(`node ${CLI} -f hash:1c5ffd2 -j`).then(x => {
    /* eslint-disable max-len */
    const y = pipe(
      JSON.parse,
      map(stripRelative)
    )(x.stdout)
    // eslint-disable-next-line
    t.deepEqual(y, [
      { date: `01-05-2018`, type: `banner` },
      {
        abbrevHash: `1c5ffd2`,
        analysis: {
          config: true,
          dependencies: true,
          gitparty: true,
          invisibles: true,
          lint: true,
          tests: false
        },
        author: `brekk`,
        authorDate: `2018-04-30 21:13:22 -0700`,
        authorDateRel: undefined,
        authorName: `brekk`,
        changes: {
          A: [
            `.babelrc`,
            `.eslintrc`,
            `.gitignore`,
            `.madgerc`,
            `.npmignore`,
            `circle.yml`,
            `gitparty.js`,
            `package-scripts.js`,
            `package.json`,
            `rollup/config.commonjs.js`,
            `rollup/config.es6.js`,
            `rollup/config.shared.js`,
            `src/alias.js`,
            `src/constants.js`,
            `src/filters.js`,
            `src/gitparty.js`,
            `src/grouping.js`,
            `src/legend.js`,
            `src/per-commit.js`,
            `src/print.js`,
            `src/utils.js`,
            `yarn.lock`
          ]
        },
        date: `01-05-2018`,
        files: [
          `.babelrc`,
          `.eslintrc`,
          `.gitignore`,
          `.madgerc`,
          `.npmignore`,
          `circle.yml`,
          `gitparty.js`,
          `package-scripts.js`,
          `package.json`,
          `rollup/config.commonjs.js`,
          `rollup/config.es6.js`,
          `rollup/config.shared.js`,
          `src/alias.js`,
          `src/constants.js`,
          `src/filters.js`,
          `src/gitparty.js`,
          `src/grouping.js`,
          `src/legend.js`,
          `src/per-commit.js`,
          `src/print.js`,
          `src/utils.js`,
          `yarn.lock`
        ],
        hash: `1c5ffd2`,
        ms: 1525148002000,
        status: [
          `A`,
          `A`,
          `A`,
          `A`,
          `A`,
          `A`,
          `A`,
          `A`,
          `A`,
          `A`,
          `A`,
          `A`,
          `A`,
          `A`,
          `A`,
          `A`,
          `A`,
          `A`,
          `A`,
          `A`,
          `A`,
          `A`
        ],
        subject: `initial commit`,
        type: `commit`
      }
    ])
    t.end()
  })
  /* eslint-enable max-len */
})
/* eslint-disable fp/no-delete */
/* eslint-disable require-jsdoc */
function stripRelative(x) {
  const y = Object.assign({}, x)
  delete y.authorDateRel
  return x && x.authorDateRel ? y : x
}
/* eslint-enable fp/no-delete */
/* eslint-enable require-jsdoc */

test.cb(`gitparty -j -f "files:**/package.json#date:01-05-2018"`, t => {
  t.plan(1)
  execa
    .shell(`node ${CLI} -j -f "files:**/package.json#date:01-05-2018"`)
    .then(x => {
      const y = pipe(
        JSON.parse,
        map(stripRelative)
      )(x.stdout)
      t.deepEqual(y, [
        { date: `01-05-2018`, type: `banner` },
        {
          abbrevHash: `852f7ac`,
          analysis: {
            config: true,
            dependencies: true,
            gitparty: true,
            invisibles: false,
            lint: false,
            tests: false
          },
          author: `brekk`,
          authorDate: `2018-05-01 07:48:20 -0700`,
          authorName: `brekk`,
          changes: {
            M: [
              `package.json`,
              `src/filters.js`,
              `src/gitparty.js`,
              `src/grouping.js`,
              `src/print.js`,
              `src/utils.js`,
              `yarn.lock`
            ]
          },
          date: `01-05-2018`,
          files: [
            `package.json`,
            `src/filters.js`,
            `src/gitparty.js`,
            `src/grouping.js`,
            `src/print.js`,
            `src/utils.js`,
            `yarn.lock`
          ],
          hash: `852f7ac`,
          ms: 1525186100000,
          status: [`M`, `M`, `M`, `M`, `M`, `M`, `M`],
          subject: `add blob matching, start to clean up legend makery`,
          type: `commit`
        },
        {
          abbrevHash: `322b8d0`,
          analysis: {
            config: true,
            dependencies: true,
            gitparty: true,
            invisibles: false,
            lint: false,
            tests: false
          },
          author: `brekk`,
          authorDate: `2018-05-01 07:05:16 -0700`,
          authorName: `brekk`,
          changes: { M: [`package.json`, `src/gitparty.js`, `yarn.lock`] },
          date: `01-05-2018`,
          files: [`package.json`, `src/gitparty.js`, `yarn.lock`],
          hash: `322b8d0`,
          ms: 1525183516000,
          status: [`M`, `M`, `M`],
          subject: `cleanups and more fp`,
          type: `commit`
        },
        {
          abbrevHash: `58b0786`,
          analysis: {
            config: true,
            dependencies: true,
            gitparty: true,
            invisibles: false,
            lint: false,
            tests: false
          },
          author: `brekk`,
          authorDate: `2018-04-30 21:56:57 -0700`,
          authorName: `brekk`,
          changes: {
            M: [
              `package.json`,
              `src/gitparty.js`,
              `src/grouping.js`,
              `src/legend.js`,
              `src/print.js`,
              `src/utils.js`,
              `yarn.lock`
            ]
          },
          date: `01-05-2018`,
          files: [
            `package.json`,
            `src/gitparty.js`,
            `src/grouping.js`,
            `src/legend.js`,
            `src/print.js`,
            `src/utils.js`,
            `yarn.lock`
          ],
          hash: `58b0786`,
          ms: 1525150617000,
          status: [`M`, `M`, `M`, `M`, `M`, `M`, `M`],
          subject: `better`,
          type: `commit`
        },
        {
          abbrevHash: `1c5ffd2`,
          analysis: {
            config: true,
            dependencies: true,
            gitparty: true,
            invisibles: true,
            lint: true,
            tests: false
          },
          author: `brekk`,
          authorDate: `2018-04-30 21:13:22 -0700`,
          authorName: `brekk`,
          changes: {
            A: [
              `.babelrc`,
              `.eslintrc`,
              `.gitignore`,
              `.madgerc`,
              `.npmignore`,
              `circle.yml`,
              `gitparty.js`,
              `package-scripts.js`,
              `package.json`,
              `rollup/config.commonjs.js`,
              `rollup/config.es6.js`,
              `rollup/config.shared.js`,
              `src/alias.js`,
              `src/constants.js`,
              `src/filters.js`,
              `src/gitparty.js`,
              `src/grouping.js`,
              `src/legend.js`,
              `src/per-commit.js`,
              `src/print.js`,
              `src/utils.js`,
              `yarn.lock`
            ]
          },
          date: `01-05-2018`,
          files: [
            `.babelrc`,
            `.eslintrc`,
            `.gitignore`,
            `.madgerc`,
            `.npmignore`,
            `circle.yml`,
            `gitparty.js`,
            `package-scripts.js`,
            `package.json`,
            `rollup/config.commonjs.js`,
            `rollup/config.es6.js`,
            `rollup/config.shared.js`,
            `src/alias.js`,
            `src/constants.js`,
            `src/filters.js`,
            `src/gitparty.js`,
            `src/grouping.js`,
            `src/legend.js`,
            `src/per-commit.js`,
            `src/print.js`,
            `src/utils.js`,
            `yarn.lock`
          ],
          hash: `1c5ffd2`,
          ms: 1525148002000,
          status: [
            `A`,
            `A`,
            `A`,
            `A`,
            `A`,
            `A`,
            `A`,
            `A`,
            `A`,
            `A`,
            `A`,
            `A`,
            `A`,
            `A`,
            `A`,
            `A`,
            `A`,
            `A`,
            `A`,
            `A`,
            `A`,
            `A`
          ],
          subject: `initial commit`,
          type: `commit`
        }
      ])
      t.end()
    })
})
