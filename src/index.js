#!/usr/bin/env node
import path from "path"
import parseArgs from "yargs-parser"
import {
  ARGV_CONFIG,
  AUTHOR_LENGTH,
  SUBJECT_LENGTH,
  BANNER_INDENT,
  BANNER_LENGTH,
  PARTYFILE
} from "./constants"
import { processAndPrintWithConfig, createADefaultConfigFile } from "./gitparty"
import { flagify } from "./utils"

const flags = flagify(ARGV_CONFIG.alias)

const config = parseArgs(process.argv.slice(2), ARGV_CONFIG)
if (config.i) {
  createADefaultConfigFile().fork(
    // eslint-disable-next-line no-console
    () => console.warn(`Unable to create a .gitpartyrc file`),
    // eslint-disable-next-line no-console
    () => console.log(`Created a .gitpartyrc file`)
  )
} else if (config.h) {
  /* eslint-disable no-console */
  /* eslint-disable max-len */
  console.log(`
# gitparty! it's a party for your git log

${flags.h}
  it's this thing you're reading now!

## configuration

${flags.o}
  write the results to a file. usually useful in concert with the --json flag
${flags.j}
  return JSON output instead of converting to colorized strings
${flags.r}
  choose a different git repository
${flags.n}
  choose a number of total commits (default: 100)

## filtering

${flags.a}
  merge commits if the authors are the same and the commit dates are the same
${flags.m}
  merge commits beginning with the string 'Merge '
${flags.f}
  filter commits based on a simple 'key:value' / 'key:value#key2:value2' base syntax:
    * -f "hash:80ca7f7" / -f "date:20-05-2018"
      lookup by exact string matching (default)
    * -f "subject:fix~"
      lookup by looser indexOf matching when there is a tilde "~" character at the end of the value
    * -f "subject:fix~#date:20-05-2018"
      lookup with multiple facets, separated by a hash "#" symbol
    * -f "author:brekk"
      when filtering by author, the alias lookup (if aliases have been defined) is used
    * -f "files:**/src/*.spec.js"
      when there are asterisks present in the value side (after the ":")
      and the key is an array, glob-style matching is performed
    * -f "analysis.config:true"
      when there is a period "." in the key, nested-key lookups will be performed
    * -f "x:true" / -f "x:false"
      when the value is either the literal string "true" or "false", it will be coerced
      into a boolean

## formatting

${flags.l}
  set a max width for author length (default: ${AUTHOR_LENGTH})
${flags.s}
  set a max width for subject length (default: ${SUBJECT_LENGTH})
${flags.b}
  set a max width for banner length (default: ${BANNER_LENGTH})
${flags.i}
  set a max width for banner indent (default: ${BANNER_INDENT})

## aliases

If author aliases are present in the ${PARTYFILE}, they will be used to identify authors in any gitparty functionality.

e.g. in the ${PARTYFILE}:

aliases:
  brekk 🔥 :
  - brekk
  - Brekk

Will result in the key (with the emoji) in place of any matched authors.
`)
  /* eslint-enable max-len */
} else {
  const input = path.resolve(process.cwd(), config.config || `./.gitpartyrc`)
  processAndPrintWithConfig(config, input)
}
/* eslint-enable no-console */
